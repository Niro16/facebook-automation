
from selenium import webdriver
import ConfigLib as ConfigLib
from time import sleep
from CustomLogger import getLogger
import random
from selenium.webdriver.chrome.options import Options
import Db_lib_facebook as Db_lib_facebook
import urllib.request
from apscheduler.schedulers.blocking import BlockingScheduler
from datetime import timedelta
import datetime
import os
import time
logger = getLogger()



class FacebookAutomator:
    def __init__(self):
        self.option = Options()
        self.option.add_argument("--disable-infobars")
        self.option.add_argument("start-maximized")
        self.option.add_argument("--disable-extensions")

        # Pass the argument 1 to allow and 2 to block
        self.option.add_experimental_option("prefs", { "profile.default_content_setting_values.notifications": 1 })

        self.driver = webdriver.Chrome(chrome_options=self.option,executable_path=ConfigLib.driver_path())
        self.driver.get(ConfigLib.open_page())
        self.max_wait = 10
        self.min_wait = 4
        self.randomWait = True


    def self_distroy(self):

        self.driver.quit()

    def element_by_xpath(self, where,what_type,text):
        if self.randomWait:
            sleep(random.randint(self.min_wait, self.max_wait))
        sleep(1)
        return self.driver.find_element_by_xpath("//"+where+"[@"+what_type+"='"+text+"']")

    def element_with_text_and_xpath(self,where,text):
        if self.randomWait:
            sleep(random.randint(self.min_wait, self.max_wait))
        sleep(1)
        return self.driver.find_element_by_xpath("//"+where+"[.='"+text+"']")

    def element_with_text_and_xpath_not_using_dot(self,where,text):
        if self.randomWait:
            sleep(random.randint(self.min_wait, self.max_wait))
        sleep(1)
        return self.driver.find_element_by_xpath("//"+where+"[text()='"+text+"']")


    def more_then_one_element_with_text_and_xpath(self,where,text):
        if self.randomWait:
            sleep(random.randint(self.min_wait, self.max_wait))
        sleep(1)
        return self.driver.find_elements_by_xpath("//"+where+"[.='"+text+"']")

    def element_with_text_and_xpath_toadd_pic_and_video(self,where):
        if self.randomWait:
            sleep(random.randint(self.min_wait, self.max_wait))
        sleep(1)
        return self.driver.find_element_by_xpath("//"+where+"[.='Photo/Video']/following-sibling::div/input")

    def element_by_link_text(self,text):
        if self.randomWait:
            sleep(random.randint(self.min_wait, self.max_wait))
        sleep(1)
        return self.driver.find_element_by_link_text(text)

    def elements_by_class(self, class_name):
        if self.randomWait:
            sleep(random.randint(self.min_wait, self.max_wait))
        sleep(1)
        return self.driver.find_elements_by_class_name(class_name)


    def elements_with_id(self, id):
        if self.randomWait:
            sleep(random.randint(self.min_wait, self.max_wait))
        return self.driver.find_elements_by_id(id)
    
    def element_with_id(self, id):
        if self.randomWait:
            sleep(random.randint(self.min_wait, self.max_wait))
        sleep(1)
        return self.driver.find_element_by_id(id)

    def random_scroll_page(self):
        if self.randomWait:
            sleep(random.randint(self.min_wait, self.max_wait))
        for i in range(0, random.randint(1,6)):
            try:
                random_y=random.randint(self.min_wait, self.max_wait)*100
                random_y=str(random_y)
                self.driver.execute_script("window.scrollTo(0, "+random_y+")") 
                sleep(random.randint(self.min_wait, self.max_wait))
            except Exception as e:
                logger.info('Failed to scroll.')
        

    def login_on_facebook(self,email,password):
        email_login='email'
        pass_login='password'
        connect_button='submit'
        self.element_by_xpath('input','type',email_login).send_keys(email)
        self.element_by_xpath('input','type',pass_login).send_keys(password)
        self.element_by_xpath('input','type',connect_button).click()
        sleep(10)
        self.random_scroll_page()

    def accept_pages(self):
        pages='left_nav_item_Pages'
        invitations='Invitations'
        accept_button='Accept'
        back_to_main_page='https://www.facebook.com/?ref=logo'

        self.element_by_xpath('a','data-testid',pages).click()
        self.element_with_text_and_xpath_not_using_dot('div',invitations).click()
        count=len(self.more_then_one_element_with_text_and_xpath('button',accept_button))
        while count!=0:
            self.element_with_text_and_xpath('button',accept_button).click()
            count=count-1
        self.element_by_xpath('a','href',back_to_main_page).click()


    def enter_on_client_page(self,page_name):
        pages='left_nav_item_Pages'
        all_pages='Your Pages'

        self.element_by_xpath('a','data-testid',pages).click()


        self.element_with_text_and_xpath('div',all_pages).click()
        self.element_with_text_and_xpath('a',page_name).click()

    def post(self  , description):
        description_box='Write a post...'
        post_button_finish='Post'
        back_to_main_page='https://www.facebook.com/?ref=logo'
        folder_photos=ConfigLib.folder_photo()
        self.element_by_xpath('textarea','title',description_box).send_keys(description)

        for photo_video in os.listdir(folder_photos):
            time.sleep(3)
            self.element_with_text_and_xpath_toadd_pic_and_video('div').send_keys(folder_photos+"/"+photo_video)
            time.sleep(3)


        self.element_with_text_and_xpath('span',post_button_finish).click()
        time.sleep(10)
        self.element_by_xpath('a','href',back_to_main_page).click()


    def download(self,urls): 
        count=1
        urls=urls.split(" ")       
        for url in urls:
            urllib.request.urlretrieve(url,ConfigLib.folder_photo()+"/"+str(count))
            count=count+1

def starting_process():
    logger.info("Job ended  <--")

    robot=FacebookAutomator()
    robot.login_on_facebook(ConfigLib.facebook_email(),ConfigLib.facebook_password())
    robot.accept_pages() 

    current_time = datetime.datetime.now(datetime.timezone.utc).astimezone()
    time_plus_five_minutes=  current_time + datetime.timedelta(minutes=5)

    #aici apelam baza de date si luam informatiile despre toti clientii care vor sa posteze ceva
    eventslist=Db_lib_facebook.search_new_events(time_plus_five_minutes)
    for events in eventslist:
        try:
            folder_photos=ConfigLib.folder_photo()
            page_name=events[2]
            desription=events[4]
            event_id=events[0]
            downloaded=0
            robot.download(events[3])
            downloaded=1
            robot.enter_on_client_page(page_name)
            robot.post(desription)
            Db_lib_facebook.uppdate_data_base(event_id,'finished')
        except Exception as e:
            Db_lib_facebook.uppdate_data_base(event_id,'failed')
            logger.info("Attempt  to post FAILED :"+e)
        if downloaded==1:
                for photo_video in os.listdir(folder_photos): 
                    os.remove(folder_photos+"/"+photo_video)    

    robot.self_distroy()
    logger.info("Job ended  <--")

#if __name__ == "__main__":
#    sched = BlockingScheduler()
#    sched.add_job(starting_process, 'interval', minutes=5)
#    sched.start()
